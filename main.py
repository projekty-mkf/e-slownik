#!~/repo/e-slownik/venv/bin/python3
from PyQt5 import QtWidgets
from GUI.files import db, glowna, login, rejestracja, resource, uwaga, wyjatki
from GUI.files.db import User, session, Dictionary
from werkzeug.security import generate_password_hash, check_password_hash
from email_validator import validate_email, EmailNotValidError
import setup
from sqlalchemy.sql import select


class Home(glowna.Ui_MainWindow):
    @staticmethod
    def exit():
        session.close()
        app.exit(app.exec_())

    def find(self, fraza_wysz, definicja_wysz):
        self.fraza_wysz = fraza_wysz
        self.definicja_wysz = definicja_wysz
        uz_wyszukaj = session.query(Dictionary).filter_by(phrase=fraza_wysz).first()
        ul_wyszukaj = session.query(Dictionary).filter_by(definition=definicja_wysz).first()
        if uz_wyszukaj:
            self.load(Dictionary.phrase == fraza_wysz, Dictionary.definition == uz_wyszukaj.definition)
            self.lineEdit.setText('')
            self.lineEdit_2.setText('')
        elif ul_wyszukaj:
            self.load(Dictionary.definition == definicja_wysz, Dictionary.definition == ul_wyszukaj.definition)
            self.lineEdit.setText('')
            self.lineEdit_2.setText('')
        elif uz_wyszukaj and ul_wyszukaj:
            self.load(Dictionary.phrase == fraza_wysz, Dictionary.definition == uz_wyszukaj.definition)
            self.load(Dictionary.definition == definicja_wysz, Dictionary.definition == ul_wyszukaj.definition)
            self.lineEdit.setText('')
            self.lineEdit_2.setText('')
        else:
            uwagain.start('Taka fraza albo/ i definicja nie istnieje')
            self.lineEdit.setText('')
            self.lineEdit_2.setText('')

    def load_all(self):
        self.tableWidget.setColumnCount(5)
        self.tableWidget.setRowCount(0)
        dane = select(Dictionary.id, Dictionary.phrase, Dictionary.definition, Dictionary.data, Dictionary.author)
        r = session.execute(dane)
        for row_number, row_data in enumerate(r):
            self.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
        self.tableWidget.resizeColumnsToContents()

    def load(self, warunek1, warunek2):
        self.tableWidget.setColumnCount(5)
        self.tableWidget.setRowCount(0)
        dane = select(Dictionary.id,
                      Dictionary.phrase,
                      Dictionary.definition,
                      Dictionary.data,
                      Dictionary.author).where(warunek1, warunek2)
        r = session.execute(dane)
        for row_number, row_data in enumerate(r):
            self.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
        self.tableWidget.resizeColumnsToContents()

    def add(self, fraza, definicja):
        import datetime
        data = datetime.datetime.now().strftime("%H:%M %d-%m-%Y")
        dane = Dictionary(phrase=fraza, definition=definicja, data=data, author=loginin.user_login)
        session.add(dane)
        session.commit()
        uwagain.start(f'Dodano {fraza}')
        self.load_all()
        self.lineEdit_7.setText('')
        self.lineEdit_8.setText('')

    def edit(self, stara_fraza, nowa_fraza, definicja):
        x = session.query(Dictionary).filter_by(phrase=stara_fraza).first()
        x.phrase = nowa_fraza
        x.definition = definicja
        session.commit()
        self.load_all()
        self.lineEdit_7.setText('')
        self.lineEdit_8.setText('')

    def remove(self, fraza, fraza_pow):
        if fraza != '' and fraza_pow != '':
            try:
                wyjatki.SprawdzCzyObieFrazySaTakieSame(fraza, fraza_pow)
                session.query(Dictionary).filter(Dictionary.phrase == fraza).delete()
                session.commit()
                self.lineEdit_12.setText('')
                self.lineEdit_13.setText('')
                uwagain.start(f'Usunięto {fraza}')
                self.load_all()
            except wyjatki.Fraza:
                uwagain.start('fraza nie jest taka sama.')
        else:
            uwagain.start('Najpierw wpisz fraze')

    def phrase_of_the_day(self):
        import datetime
        data = datetime.datetime.now().strftime("%d")
        try:
            wyjatki.SprawdzDzienTygodnia(data)
        except wyjatki.FrazaDnia as d:
            self.fraza = d.text


class Login(login.Ui_MainWindow):
    def __init__(self):
        self.user_login = ''

    @staticmethod
    def reg_window():
        rej.setupUi(rejestr)

    def login(self, user_login, user_haslo):
        self.user_login = user_login
        self.log()
        if user_login != '' and user_haslo != '':
            try:
                uz_login = session.query(User).filter_by(login=user_login).first()
                if uz_login and wyjatki.SprawdzCzyHasloJestPoprawne(uz_login.haslo, user_haslo):
                    glownain.setupUi(Main)
                    rejestr.close()
                    Main.show()
                    glownain.load_all()
                else:
                    raise wyjatki.Hash('tak', 'nie')
            except wyjatki.Hash:
                self.log_exe()
                uwagain.start('Sprawdz dane logowania i spróbuj ponownie')
        else:
            self.log_exe()
            uwagain.start('Pola nie mogą być puste')


class Register(rejestracja.Ui_MainWindow):
    def zarejestruj(self, user_login, user_pass, user_email):
        while True:
            self.log()
            if user_login and user_pass and user_email:
                lo = session.query(User).filter_by(login=user_login).first()
                if lo:
                    uwagain.start('Login jest już w systemie')
                    self.log_exe()
                    break

                try:
                    email = validate_email(user_email)
                    user_email = email.email
                except EmailNotValidError:
                    uwagain.start('Niepoprawny email')
                    self.log_exe()
                    break

                em = session.query(User).filter_by(email=user_email).first()

                if em:
                    uwagain.start('Email jest już w systemie')
                    self.log_exe()
                    break

                import datetime
                data = datetime.datetime.now().strftime("%H:%M %d-%m-%Y")
                user = User(login=user_login,
                            haslo=generate_password_hash(user_pass, method='sha256'),
                            email=user_email,
                            data=data)
                session.add(user)
                session.commit()
                uwagain.start('Zarejestrowano !!')
                loginin.setupUi(rejestr)
                self.log_exe()
                break
            else:
                uwagain.start('Żadne pole nie może być puste')
                self.log_exe()
                break


class Attention(uwaga.Ui_Dialog):
    @staticmethod
    def close():
        Dialog.close()

    @staticmethod
    def start(mess):
        uwagain.mess = mess
        uwagain.setupUi(Dialog)
        Dialog.exec_()


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)

    Main = QtWidgets.QMainWindow()
    Dialog = QtWidgets.QDialog()
    rejestr = QtWidgets.QMainWindow()

    glownain = Home()
    loginin = Login()
    rej = Register()
    uwagain = Attention()

    glownain.ver = setup.app.get('ver')

    loginin.setupUi(rejestr)

    glownain.phrase_of_the_day()

    rejestr.show()

    sys.exit(app.exec_())
